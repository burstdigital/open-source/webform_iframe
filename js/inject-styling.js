/* eslint-disable no-var, func-names, strict */
(function() {
  'use strict';

  var styleNode;

  function onMessage(event) {
    if (!event.data || event.data.type !== 'INJECT_STYLING') return;
    if (typeof event.data.css !== 'string')
      throw new Error('data.css should be a string.');

    if (!styleNode) {
      styleNode = document.createElement('style');
      document.body.appendChild(styleNode);
    }
    styleNode.innerHTML = event.data.css;
  }

  window.addEventListener('message', onMessage, false);
  window.parent.postMessage({ type: 'INJECT_STYLING_READY' }, '*');
})();
