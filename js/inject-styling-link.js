/* eslint-disable no-var, func-names, strict */
(function() {
  'use strict';

  var linkNode;

  function onMessage(event) {
    if (!event.data || event.data.type !== 'INJECT_STYLING_LINK') return;
    if (typeof event.data.cssPath !== 'string')
      throw new Error('data.css should be a string.');

    if (!linkNode) {
      linkNode = document.createElement('link');
      linkNode.rel = 'stylesheet';
      linkNode.onload = function() {
        window.parent.postMessage({ type: 'INJECT_STYLING_LINK_LOADED' }, '*');
      };
      document.head.appendChild(linkNode);
    }

    linkNode.href = event.data.cssPath;
  }

  window.addEventListener('message', onMessage, false);
  window.parent.postMessage({ type: 'INJECT_STYLING_READY' }, '*');
})();
