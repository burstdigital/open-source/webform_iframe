/* eslint-disable no-var, func-names, strict */
(function ($, Drupal) {

  /*
  This has been copied from drupal/web/modules/contrib/webform/js/webform.ajax.js
  with the only exception that the default redirect is being replaced by a postMessage.
  */

  Drupal.AjaxCommands.prototype.webformRefresh = function (ajax, response, status) {
    // Disable the submit button during redirect
    // Drupal automatically re-enables the button after the ajax call comes back.
    $('.js-form-submit').prop('disabled', true);

    // Get URL path name.
    // @see https://stackoverflow.com/questions/6944744/javascript-get-portion-of-url-path
    var a = document.createElement('a');
    a.href = response.url;
    if (a.pathname === window.location.pathname && $('.webform-ajax-refresh').length) {
      updateKey = (response.url.match(/[?|&]update=([^&]+)($|&)/)) ? RegExp.$1 : null;
      addElement = (response.url.match(/[?|&]add_element=([^&]+)($|&)/)) ? RegExp.$1 : null;
      $('.webform-ajax-refresh').click();
    }
    else {
      // Clear unsaved information flag so that the current webform page
      // can be redirected.
      // @see Drupal.behaviors.webformUnsaved.clear
      if (Drupal.behaviors.webformUnsaved) {
        Drupal.behaviors.webformUnsaved.clear();
      }

      // Use the current url to strip the hostname from the redirect url in Drupal
      var redirectUrl = response.url;
      var isExternal = true;
      var currentUrl =  window.location.protocol + '//' + window.location.hostname + window.location.port;

      if (redirectUrl.indexOf(currentUrl) > -1) {
        redirectUrl = response.url.replace(currentUrl, '');
        isExternal = false;
      }

      // Send FormData to the parent frame
      var formData = {};

      if ($(ajax.$form.get(0)).length > 0) {
        formData = $(ajax.$form.get(0)).serializeArray().reduce(function(result, current){
          if (!result[current.name]) {
            result[current.name] = current.value;
          }

          return result;
        }, formData);
      }

      window.parent.postMessage({ type: 'FORM_SUBMISSION_COMPLETE_REDIRECT', redirectUrl: redirectUrl, isExternal: isExternal, formData: formData }, '*');
    }
  };

})(jQuery, Drupal);
